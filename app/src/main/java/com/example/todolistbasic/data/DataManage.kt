package com.example.todolistbasic.data

import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.remember
import com.example.todolistbasic.models.Task

val tasksList  =  mutableStateListOf<Task>(Task(id = "1", name = "Crear " +
        "web", description = "con react y nestjs"),
    Task(id = "2", name = "Crear app", description = "similar a uber"),
    Task(id = "3", name = "Crear app desktop", description = "entregar en un mes"),
    Task(id = "4", name = "Crear webservice", description = "con springboot"),)

