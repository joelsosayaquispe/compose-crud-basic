package com.example.todolistbasic.ui.views

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.example.todolistbasic.data.tasksList
import com.example.todolistbasic.models.Task
import java.util.UUID

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TaskFormView(navController: NavHostController, id: String?) {
    var tfName by remember { mutableStateOf<String>("") }
    var tfDescription by remember { mutableStateOf<String>("") }
    var findTask by remember {mutableStateOf<Task?>(null)};

    fun saveTask(task: Task) {
        tasksList.add(task)
        navController.popBackStack();
    }

    fun update(task:Task){
        val index = tasksList.indexOf(findTask);
        task.id = findTask!!.id;
        tasksList[index] = task;
        navController.popBackStack();
    }

    fun handleForm(task:Task){
        if(id != null){
            update(task);
        }else{
            saveTask(task)
        }
    }

    LaunchedEffect(Unit){

        if(!id.isNullOrBlank()){
            findTask = tasksList.first { data -> data.id == id };
            tfName = findTask!!.name;
            tfDescription = findTask!!.description;
        }

    }

    Column(modifier = Modifier.padding(15.dp)) {
        IconButton(onClick = {
            navController.popBackStack()
        }) {
            Image(imageVector = Icons.Default.ArrowBack, contentDescription = "" )
        }
        if (id != null) {
            Text(text = "Editar Tarea", fontSize = 23.sp, fontWeight = FontWeight.Bold)
        } else {
            Text(text = "Agregar Tarea", fontSize = 23.sp, fontWeight = FontWeight.Bold)
        }

        TextField(modifier = Modifier.fillMaxWidth(), value = tfName, onValueChange = {
            tfName = it
        }, placeholder = { Text(text = "Crear web...") }, label = { Text(text = "Nombre") })

        Spacer(modifier = Modifier.height(10.dp))
        TextField(modifier = Modifier.fillMaxWidth(), value = tfDescription, onValueChange = {
            tfDescription = it
        }, placeholder = { Text(text = "Entregar mañana....") }, label = {
            Text(
                text =
                "Descripción"
            )
        })

        Button(onClick = {
            val newTask =
                Task(id = UUID.randomUUID().toString(), name = tfName, description = tfDescription);
            handleForm(newTask)
        }) {
            Text(text = "Guardar")
        }

    }
}