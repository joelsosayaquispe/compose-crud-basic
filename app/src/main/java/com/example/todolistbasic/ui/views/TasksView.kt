package com.example.todolistbasic.ui.views

import android.annotation.SuppressLint
import android.view.GestureDetector
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material3.Card
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.NavHostController

import com.example.todolistbasic.data.tasksList
import com.example.todolistbasic.models.Task
import com.example.todolistbasic.route.Routes


@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter", "UnrememberedMutableState")
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TasksView(navController:NavHostController) {

    fun navigateFormTask(){
        navController.navigate(Routes.CreateTask.route);
    }

    fun deleteTask(index: Int){
        tasksList.removeAt(index);
    }

   Scaffold (floatingActionButton = {
       IconButton(onClick = {
           navigateFormTask()
       }) {
           Icon(imageVector = Icons.Default.Add, contentDescription ="" )
       }
   }){
       Column(modifier = Modifier.padding(horizontal = 15.dp) ){
           Text(text = "Mis tareas", fontSize = 22.sp, fontWeight = FontWeight.Bold)
           LazyColumn(content = {
               items(tasksList.size, itemContent = { index ->

                   Card(onClick = {
                                  navController.navigate(Routes.UpdateTask.createRoute
                                      (tasksList[index].id
                                              ))
                   },modifier = Modifier
                       .fillMaxWidth()
                       .padding(10.dp)) {

                           Row(modifier = Modifier
                               .padding(10.dp)
                               .fillMaxWidth(),Arrangement.SpaceBetween) {
                               Column {
                                   Text(text = tasksList[index].name, fontWeight = FontWeight.Bold)
                                   Text(text = tasksList[index].description)
                               }

                               IconButton(onClick = {
                                   deleteTask(index);
                               }) {
                                   Icon(imageVector = Icons.Default.Delete, contentDescription ="" )
                               }
                           }

                   }
                   Divider()
               })
           })
       }
   }
}