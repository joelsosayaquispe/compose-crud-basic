package com.example.todolistbasic.route

sealed class Routes(var route:String) {
    object  Tasks: Routes("tasks")
    object  UpdateTask: Routes("update/{id}"){
        fun createRoute(id:String) = "update/$id"
    }
    object  CreateTask: Routes("create")
}