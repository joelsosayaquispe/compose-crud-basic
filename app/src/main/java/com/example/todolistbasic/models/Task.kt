package com.example.todolistbasic.models

data class Task (
    var id:String,
    val name:String,
    val description: String
)
