package com.example.todolistbasic

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.todolistbasic.route.Routes
import com.example.todolistbasic.ui.theme.TodolistbasicTheme
import com.example.todolistbasic.ui.views.TaskFormView
import com.example.todolistbasic.ui.views.TasksView

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            TodolistbasicTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val navController = rememberNavController()
                    NavHost(navController = navController, startDestination = Routes.Tasks.route ){
                        composable(Routes.Tasks.route, content = { TasksView(navController)})
                        composable(Routes.UpdateTask.route){ backStackEntry ->
                            val id = backStackEntry.arguments?.getString("id")

                            TaskFormView(navController = navController,id= id)
                        }
                        composable(Routes.CreateTask.route){
                            TaskFormView(navController = navController, id =null )
                        }

                    }

                }
            }
        }
    }
}

@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
    Text(
        text = "Hello $name!",
        modifier = modifier
    )
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    TodolistbasicTheme {
        Greeting("Android")
    }
}